import 'package:ads/common/state_enum.dart';
import 'package:ads/domain/entities/advertisement.dart';
import 'package:ads/domain/usecases/get_ads.dart';
import 'package:flutter/foundation.dart';

class AdsNotifier extends ChangeNotifier {
  final GetAds getAds;

  AdsNotifier(this.getAds);

  RequestState _state = RequestState.empty;
  RequestState get state => _state;

  List<Advertisement> _ads = [];
  List<Advertisement> get ads => _ads;

  String _message = '';
  String get message => _message;

  Future<void> fetchAds() async {
    _state = RequestState.loading;
    notifyListeners();

    final result = await getAds.execute();

    result.fold(
      (failure) {
        _message = failure.message;
        _state = RequestState.error;
        notifyListeners();
      },
      (adsData) {
        _ads = adsData
          ..sort((a, b) => a.appearanceOrder.compareTo(b.appearanceOrder));
        _state = RequestState.loaded;
        notifyListeners();
      },
    );
  }
}
