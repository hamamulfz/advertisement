import 'package:ads/common/navigator_service.dart';
import 'package:ads/common/pref_manager.dart';
import 'package:ads/common/state_enum.dart';
import 'package:ads/domain/entities/login_data.dart';
import 'package:ads/domain/usecases/login.dart';
import 'package:ads/injection.dart';
import 'package:ads/presentation/pages/home/home_screen.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginNotifier extends ChangeNotifier {
  final LoginAuth login;

  LoginNotifier(this.login);

  RequestState _state = RequestState.empty;
  RequestState get state => _state;

  LoginData _dataLogin;
  LoginData get dataLogin => _dataLogin;

  String _message = '';
  String get message => _message;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  get scaffoldKey => _scaffoldKey;

  Future<void> postLogin(body) async {
    _state = RequestState.loading;
    notifyListeners();

    final result = await login.execute(body);

    result.fold(
      (failure) {
        _message = failure.message;
        _state = RequestState.error;
        notifyListeners();
      },
      (data) async {
        _dataLogin = data;
        _state = RequestState.loaded;
        notifyListeners();

        locator<NavigationService>().navigateAndReplaceTo(HomeScreen());

        var _initPrefManager = await SharedPreferences.getInstance();
        final pref = PrefManager(_initPrefManager);
        pref.setToken(data.token);
      },
    );
  }
}
