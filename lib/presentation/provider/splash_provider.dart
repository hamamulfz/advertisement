import 'dart:async';

import 'package:ads/common/navigator_service.dart';
import 'package:ads/common/pref_manager.dart';
import 'package:ads/injection.dart';
import 'package:ads/presentation/pages/home/home_screen.dart';
import 'package:ads/presentation/pages/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashNotifier extends ChangeNotifier {
  SplashNotifier();

  navigate() {
    Timer(Duration(seconds: 2), () async {
      var _initPrefManager = await SharedPreferences.getInstance();
      final pref = PrefManager(_initPrefManager);

      final token = pref.getToken();
      dynamic dest = LoginScreen();
      if (token != null) {
        dest = HomeScreen();
      }
      locator<NavigationService>().navigateAndReplaceTo(dest);
    });
  }
}
