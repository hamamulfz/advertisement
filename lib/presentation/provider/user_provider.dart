import 'package:ads/common/state_enum.dart';
import 'package:ads/domain/entities/user_data.dart';
import 'package:ads/domain/usecases/get_user.dart';
import 'package:flutter/foundation.dart';

class UserNotifier extends ChangeNotifier {
  final GetUser getUser;

  UserNotifier(this.getUser);

  RequestState _state = RequestState.empty;
  RequestState get state => _state;

  UserData _user ;
  UserData get user => _user;

  String _message = '';
  String get message => _message;

  Future<void> fetchUser() async {
    _state = RequestState.loading;
    notifyListeners();

    final result = await getUser.execute();

    result.fold(
      (failure) {
        _message = failure.message;
        _state = RequestState.error;
        notifyListeners();
      },
      (adsData) {
        _user = adsData;
        _state = RequestState.loaded;
        notifyListeners();
      },
    );
  }
}
