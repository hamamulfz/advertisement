import 'package:ads/common/app_colors.dart';
import 'package:ads/domain/entities/advertisement.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:url_launcher/url_launcher.dart';

class AdsDetailScreen extends StatelessWidget {
  AdsDetailScreen({Key key, this.ad}) : super(key: key);
  final Advertisement ad;
  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffold,
      bottomNavigationBar: SafeArea(
          child: Container(
        height: 50,
        margin: EdgeInsets.all(20),
        child: RaisedButton(
          onPressed: () async {
            final urlString = "https://wa.me/+6288980978";
            if (await canLaunch(urlString)) {
              launch(urlString);
            } else {
              _scaffold.currentState.showSnackBar(
                  SnackBar(content: Text("Failed to launch action")));
            }
          },
          color: AppColors.primary2,
          child: Text(
            "Apply",
            style: TextStyle(
              fontSize: 18,
              letterSpacing: 2,
              color: Colors.white,
            ),
          ),
        ),
      )),
      body: Container(
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8),
                      topRight: Radius.circular(8),
                    ),
                    child: Container(
                      color: Colors.grey,
                      width: double.infinity,
                      child: Hero(
                        tag: "${ad.id}",
                        child: Image.network(
                          ad.imagePath,
                          errorBuilder: (context, a, b) {
                            return Stack(
                              children: <Widget>[
                                Container(
                                  height: 200,
                                  width: double.infinity,
                                  child: Image.asset(
                                    "assets/placeholder.gif",
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                SafeArea(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Align(
                                      alignment: Alignment.topRight,
                                      child: Image.asset(
                                        "assets/advertisements.png",
                                        width: 50,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                  CustomPadding(
                    children: [
                      Text(
                        ad.name,
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 5),
                      Container(
                        color: Colors.green.withOpacity(0.3),
                        padding:
                            EdgeInsets.symmetric(vertical: 4, horizontal: 5),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Icon(
                              Icons.bookmark_border,
                              color: Colors.green,
                              size: 10,
                            ),
                            SizedBox(width: 3),
                            Text(
                              "Best Offer",
                              style: TextStyle(
                                  color: Colors.green,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 10),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  HorizontalSeparator(),
                  CustomPadding(children: [
                    Text(
                      ad.title,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 10),
                    Html(data: ad.body),
                    SizedBox(height: 10),
                    Text(
                      ad.termsAndConditions,
                      style: TextStyle(
                        fontSize: 12,
                        color: Colors.grey,
                      ),
                    ),
                  ]),
                  HorizontalSeparator(),
                  CustomPadding(
                    children: <Widget>[
                      Text(
                        "Expiration Date",
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.grey,
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(
                        ad.formattedExpiryDate,
                        style: TextStyle(
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                  HorizontalSeparator(),
                ],
              ),
            ),
            SafeArea(
                child: Row(
              children: <Widget>[
                BackButton(),
                SizedBox(width: 10),
                Text(
                  "Back",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                )
              ],
            )),
          ],
        ),
      ),
    );
  }
}

class HorizontalSeparator extends StatelessWidget {
  const HorizontalSeparator({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 5,
      width: double.infinity,
      color: Colors.grey.withOpacity(0.2),
    );
  }
}

class CustomPadding extends StatelessWidget {
  CustomPadding({
    Key key,
    @required this.children,
  }) : super(key: key);
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: children,
      ),
    );
  }
}
