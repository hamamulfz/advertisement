import 'dart:async';

import 'package:ads/common/app_colors.dart';
import 'package:ads/common/styles.dart';
import 'package:ads/presentation/provider/splash_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.microtask(() {
      Provider.of<SplashNotifier>(context, listen: false).navigate();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primary2,
      body: Stack(
        children: <Widget>[
          Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Image.asset(
                  "assets/bullhorn.png",
                  width: 100,
                  fit: BoxFit.cover,
                ),
                SizedBox(height: 10),
                Text(
                  "AdsGo",
                  style: TextStyle(
                    fontSize: 30,
                    letterSpacing: 3,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
                SizedBox(height: 5),
                Text("coding-test", style: AppStyles.subtitle),
              ],
            ),
          ),
          Positioned(
            bottom: 10,
            left: 0,
            right: 0,
            child: SafeArea(
              child: Column(
                children: <Widget>[
                  SpinKitCircle(
                    color: Colors.white,
                  ),
                  SizedBox(height: 10),
                  Text(
                    "as part of",
                    style: AppStyles.subtitle,
                  ),
                  SizedBox(height: 2),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Image.asset(
                      "assets/company_logo_text.png",
                      width: 100,
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(height: 2),
                  Text(
                    "recruitment process",
                    style: AppStyles.subtitle,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
