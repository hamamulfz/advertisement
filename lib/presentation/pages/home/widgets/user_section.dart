import 'package:ads/common/app_colors.dart';
import 'package:ads/common/state_enum.dart';
import 'package:ads/presentation/pages/profile_page.dart';
import 'package:ads/presentation/provider/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';

class UserSection extends StatelessWidget {
  UserSection({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<UserNotifier>(
      builder: (context, provider, child) {
        if (provider.state == RequestState.loading) {
          return Container(
              child: SpinKitThreeBounce(
            color: AppColors.primary,
          ));
        }
        if (provider.state == RequestState.loaded) {
          final user = provider.user;
          return Container(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 5,
            ),
            child: Row(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: Image.network(
                    user.photo,
                    width: 50,
                    height: 50,
                    fit: BoxFit.cover,
                    errorBuilder: (context, a, b) {
                      return Container(
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          shape: BoxShape.circle,
                        ),
                        padding: EdgeInsets.all(10),
                        child: Icon(
                          Icons.person,
                          size: 20,
                        ),
                      );
                    },
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        user.userName,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black.withOpacity(0.8),
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(height: 3),
                      Text(
                        user.nationality,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                          color: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 5),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => ProfileScreen(),
                    ));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black.withOpacity(0.2),
                      ),
                      shape: BoxShape.circle,
                    ),
                    padding: EdgeInsets.all(7),
                    child: Icon(
                      Icons.settings,
                      color: Colors.black.withOpacity(0.5),
                      size: 20,
                    ),
                  ),
                ),
              ],
            ),
          );
        }

        return Container();
      },
    );
  }
}
