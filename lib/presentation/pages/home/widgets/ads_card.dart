import 'package:ads/domain/entities/advertisement.dart';
import 'package:ads/presentation/pages/ads_detail_screen.dart';
import 'package:flutter/material.dart';

class AdsCard extends StatelessWidget {
  const AdsCard({
    Key key,
    this.ad,
  }) : super(key: key);

  final Advertisement ad;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => AdsDetailScreen(
            ad: ad,
          ),
        ));
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 4),
              color: Colors.black.withOpacity(0.2),
              blurRadius: 20,
            )
          ],
        ),
        margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8),
                topRight: Radius.circular(8),
              ),
              child: Container(
                color: Colors.grey,
                height: 120,
                width: double.infinity,
                child: Hero(
                  tag: "${ad.id}",
                  child: Image.network(
                    ad.imagePath,
                    errorBuilder: (context, a, b) {
                      return Stack(
                        children: <Widget>[
                          Container(
                            height: 120,
                            width: double.infinity,
                            child: Image.asset(
                              "assets/placeholder.gif",
                              fit: BoxFit.cover,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Align(
                              alignment: Alignment.topRight,
                              child: Image.asset(
                                "assets/advertisements.png",
                                width: 50,
                                color: Colors.white,
                              ),
                            ),
                          )
                        ],
                      );
                    },
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    ad.name,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 4),
                  Align(
                    alignment: Alignment.topRight,
                    child: Text(
                      "Tap to see detail",
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
