import 'package:ads/common/state_enum.dart';
import 'package:ads/presentation/pages/home/widgets/ads_card.dart';
import 'package:ads/presentation/pages/home/widgets/deals_offer_text.dart';
import 'package:ads/presentation/pages/home/widgets/shimmer_loading.dart';
import 'package:ads/presentation/pages/home/widgets/user_section.dart';
import 'package:ads/presentation/provider/ads_provider.dart';
import 'package:ads/presentation/provider/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    Future.microtask(() {
      Provider.of<UserNotifier>(context, listen: false).fetchUser();
      Provider.of<AdsNotifier>(context, listen: false).fetchAds();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context).viewPadding.top + 20),
          child: Column(
            children: <Widget>[
              UserSection(),
              DealsOfferText(),
              Consumer<AdsNotifier>(
                builder: (context, provider, _) {
                  if (provider.state == RequestState.loading) {
                    return ListView.builder(
                      padding: EdgeInsets.zero,
                      shrinkWrap: true,
                      itemCount: 4,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return ShimmerLoading();
                      },
                    );
                  }

                  final ads = provider.ads;

                  return ListView.builder(
                    padding: EdgeInsets.zero,
                    shrinkWrap: true,
                    itemCount: ads.length,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      final ad = ads[index];
                      return AdsCard(
                        ad: ad,
                      );
                    },
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
