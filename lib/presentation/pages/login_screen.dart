import 'package:ads/common/app_colors.dart';
import 'package:ads/common/state_enum.dart';
import 'package:ads/presentation/provider/login_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  // final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Provider.of<LoginNotifier>(context, listen: false).scaffoldKey,
      body: Consumer<LoginNotifier>(
        builder: (context, provider, child) => ModalProgressHUD(
          inAsyncCall: provider.state == RequestState.loading,
          child: child,
          progressIndicator: SpinKitChasingDots(
            color: AppColors.complement
          ),
        ),
        child: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 20),
                  Image.asset(
                    "assets/bullhorn.png",
                    width: 120,
                    fit: BoxFit.cover,
                  ),
                  SizedBox(height: 20),
                  Text(
                    "Selamat datang di AdsGo",
                    style: TextStyle(
                      color: AppColors.secondary,
                      fontSize: 16,
                      letterSpacing: 2,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 20),
                  TextField(
                    controller: usernameController,
                    decoration: InputDecoration(
                      hintText: "Username",
                      border: OutlineInputBorder(),
                    ),
                  ),
                  SizedBox(height: 20),
                  TextField(
                    controller: passwordController,
                    decoration: InputDecoration(
                      hintText: "Password",
                      border: OutlineInputBorder(),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: FlatButton(
                      onPressed: () {
                        Provider.of<LoginNotifier>(context, listen: false)
                            .scaffoldKey
                            .currentState
                            .showSnackBar(
                                SnackBar(content: Text("Coming soon")));
                      },
                      child: Text(
                        "Lupa Password?",
                        style: TextStyle(
                          color: AppColors.secondary,
                          letterSpacing: 1,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.2),
                  Container(
                    height: 50,
                    width: double.infinity,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      color: AppColors.primary,
                      onPressed: () {
                        Provider.of<LoginNotifier>(context, listen: false)
                            .postLogin({
                          "userName": usernameController.text,
                          "password": passwordController.text,
                        });
                      },
                      child: Text(
                        "MASUK",
                        style: TextStyle(
                          color: Colors.white,
                          // fontSize: 16,
                          letterSpacing: 2,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    height: 50,
                    width: double.infinity,
                    child: RaisedButton(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        side: BorderSide(
                          color: AppColors.complement,
                        ),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      onPressed: () {
                        Provider.of<LoginNotifier>(context, listen: false)
                            .scaffoldKey
                            .currentState
                            .showSnackBar(
                                SnackBar(content: Text("Coming soon")));
                      },
                      child: Text(
                        "PENGGUNA BARU? DAFTAR",
                        style: TextStyle(
                          color: AppColors.secondary,
                          letterSpacing: 1,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
