import 'package:ads/common/app_colors.dart';
import 'package:ads/presentation/provider/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfileDetailScreen extends StatelessWidget {
  const ProfileDetailScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.primary2,
        title: Text("My Account"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Consumer<UserNotifier>(
          builder: (context, provider, _) => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: Image.network(
                    provider.user.photo,
                    width: 100,
                    height: 100,
                    fit: BoxFit.cover,
                    errorBuilder: (context, a, b) {
                      return Container(
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          shape: BoxShape.circle,
                        ),
                        padding: EdgeInsets.all(20),
                        child: Icon(
                          Icons.person,
                          size: 50,
                        ),
                      );
                    },
                  ),
                ),
              ),
              SizedBox(height: 10),
              Card(
                child: Container(
                  width: double.infinity,
                  padding: const EdgeInsets.all(18.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      LabelText(
                        label: "Name",
                        title: provider.user.userName,
                      ),
                      SizedBox(height: 10),
                      LabelText(
                        label: "Nationality",
                        title: provider.user.nationality,
                      ),
                      SizedBox(height: 10),
                      LabelText(
                        label: "Gender",
                        title: provider.user.gender,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class LabelText extends StatelessWidget {
  const LabelText({
    Key key,
    this.label,
    this.title,
  }) : super(key: key);

  final String label;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Text(
          label,
          style: TextStyle(
            color: Colors.grey,
            fontSize: 12,
            fontWeight: FontWeight.bold,
          ),
        ),
        Text(
          title,
          style: TextStyle(
            color: Colors.black87,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }
}
