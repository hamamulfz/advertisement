import 'package:ads/common/app_colors.dart';
import 'package:ads/common/pref_manager.dart';
import 'package:ads/presentation/pages/login_screen.dart';
import 'package:ads/presentation/pages/profile_detail_screen.dart';
import 'package:ads/presentation/provider/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.primary2,
        title: Text(
          "Profile",
          style: TextStyle(),
        ),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Consumer<UserNotifier>(
              builder: (context, provider, _) => Column(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: Image.network(
                      provider.user.photo,
                      width: 100,
                      height: 100,
                      fit: BoxFit.cover,
                      errorBuilder: (context, a, b) {
                        return Container(
                          decoration: BoxDecoration(
                            color: Colors.grey,
                            shape: BoxShape.circle,
                          ),
                          padding: EdgeInsets.all(20),
                          child: Icon(
                            Icons.person,
                            size: 50,
                          ),
                        );
                      },
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    provider.user.userName,
                    style: TextStyle(
                      color: Colors.black87,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            Builder(builder: (context) {
              return ListTile(
                title: Text("Personal Information"),
                leading: Icon(Icons.person_outline),
                trailing: Icon(Icons.arrow_forward_ios),
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ProfileDetailScreen(),
                  ));
                },
              );
            }),
            Divider(),
            ListTile(
              title: Text(
                "Log out",
                style: TextStyle(
                  color: Colors.red,
                  fontWeight: FontWeight.bold,
                ),
              ),
              leading: Icon(
                Icons.exit_to_app,
                color: Colors.red,
              ),
              onTap: () async {
                final willLogout = await showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text("LOGOUT"),
                      content: Text("Are you sure want to logout?"),
                      actions: <Widget>[
                        RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          onPressed: () {
                            Navigator.pop(context, true);
                          },
                          color: Colors.red,
                          child: Text("Continue"),
                        ),
                        OutlineButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          onPressed: () {
                            Navigator.pop(context, false);
                          },
                          child: Text("Cancel"),
                        ),
                      ],
                    );
                  },
                );

                if (willLogout == true) {
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (context) => LoginScreen()),
                      (route) => false);
                  var _initPrefManager = await SharedPreferences.getInstance();
                  final pref = PrefManager(_initPrefManager);
                  pref.logout();
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
