import 'package:ads/common/navigator_service.dart';
import 'package:ads/common/pref_manager.dart';
import 'package:ads/data/datasources/ads_datasource.dart';
import 'package:ads/data/datasources/auth_datasource.dart';
import 'package:ads/data/datasources/user_datasource.dart';
import 'package:ads/data/repositories/ads_repositories_impl.dart';
import 'package:ads/data/repositories/auth_repositories_impl.dart';
import 'package:ads/data/repositories/user_repositories_impl.dart';
import 'package:ads/domain/repositories/ads_repositories.dart';
import 'package:ads/domain/repositories/auth_repositories.dart';
import 'package:ads/domain/repositories/user_repositories.dart';
import 'package:ads/domain/usecases/get_ads.dart';
import 'package:ads/domain/usecases/get_user.dart';
import 'package:ads/domain/usecases/login.dart';
import 'package:ads/presentation/provider/ads_provider.dart';
import 'package:ads/presentation/provider/login_provider.dart';
import 'package:ads/presentation/provider/splash_provider.dart';
import 'package:ads/presentation/provider/user_provider.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

final locator = GetIt.instance;

Future<void> init() async {
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => PrefManager(locator()));

  // provider
  locator.registerFactory(
    () => SplashNotifier(),
  );
  locator.registerFactory(
    () => LoginNotifier(locator()),
  );
  locator.registerFactory(
    () => UserNotifier(locator()),
  );
  locator.registerFactory(
    () => AdsNotifier(locator()),
  );

  // usecase
  locator.registerLazySingleton(() => GetAds(locator()));
  locator.registerLazySingleton(() => GetUser(locator()));
  locator.registerLazySingleton(() => LoginAuth(locator()));

  // repository
  locator.registerLazySingleton<AdsRepository>(
    () => AdsRepositoryImpl(
      dataSource: locator(),
    ),
  );
  locator.registerLazySingleton<UserRepository>(
    () => UserRepositoryImpl(
      dataSource: locator(),
    ),
  );
  locator.registerLazySingleton<AuthRepository>(
    () => AuthRepositoryImpl(
      dataSource: locator(),
    ),
  );

  // data sources
  locator.registerLazySingleton<AdsDataSource>(
      () => AdsDataSourceDataSourceImpl(client: locator()));
  locator.registerLazySingleton<AuthDataSource>(
      () => AuthDataSourceDataSourceImpl(client: locator()));
  locator.registerLazySingleton<UserDataSource>(
      () => UserDataSourceDataSourceImpl(client: locator()));

  // external
  locator.registerLazySingleton(() => http.Client());
  var _initPrefManager = await SharedPreferences.getInstance();
  locator.registerLazySingleton(() => _initPrefManager);
}
