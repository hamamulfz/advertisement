class ApiUrl {
  static const baseUrl = 'https://51eea27e-6354-4d21-bfba-cf995372ce4e.mock.pstmn.io';

  static final login = baseUrl + "/login";
  static final getUser = baseUrl + "/getUser";
  static final getAdvertisment = baseUrl + "/getAdvertisement";
}
