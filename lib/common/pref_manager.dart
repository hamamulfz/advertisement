import 'package:shared_preferences/shared_preferences.dart';

class PrefManager {
  String kToken = "token";

  SharedPreferences preferences;

  PrefManager(this.preferences);

  // Token
  Future<bool> setToken(String value) async =>
      await preferences.setString(kToken, value);

  String getToken() => preferences.getString(kToken);

  getTokenBody() {
    final token = getToken();
    final body = {"token": token};
    return body;
  }

  void logout() async {
    await preferences.remove(kToken);
    await preferences.reload();
  }
  // logout() async => await preferences.clear();
}
