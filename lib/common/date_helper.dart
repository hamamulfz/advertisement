import 'package:intl/intl.dart';

class DateHelper {
  static String convertDateFormat(String dateTime) {
    return DateFormat("dd MMMM yyyy HH:mm").format(DateTime.parse(dateTime));
  }
}
