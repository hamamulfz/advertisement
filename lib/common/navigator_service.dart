import 'package:flutter/material.dart';

class NavigationService {
  final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();

  Future<dynamic> navigateTo(destination) {
    return navigatorKey.currentState.push(MaterialPageRoute(
      builder: (context) => destination,
    ));
  }

  Future<dynamic> navigateAndReplaceTo(destination) {
    return navigatorKey.currentState.pushReplacement(MaterialPageRoute(
      builder: (context) => destination,
    ));
  }

  Future<dynamic> navigateAndReplaceAllTo(destination) {
    return navigatorKey.currentState.pushAndRemoveUntil(
        MaterialPageRoute(
          builder: (context) => destination,
        ),
        (route) => false);
  }

  Future<dynamic> navigateToNamed(String routeName) {
    return navigatorKey.currentState.pushNamed(routeName);
  }
}
