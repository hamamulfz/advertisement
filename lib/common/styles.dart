import 'package:ads/common/app_colors.dart';
import 'package:flutter/material.dart';

class AppStyles {
  static final subtitle = TextStyle(
    fontSize: 12,
    color: AppColors.complement,
  );
}
