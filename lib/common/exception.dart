class ServerException implements Exception {}

class Unauthenticated implements Exception {}

class Unauthorized implements Exception {}

class DatabaseException implements Exception {
  final String message;

  DatabaseException(this.message);
}

class CacheException implements Exception {
  final String message;

  CacheException(this.message);
}
