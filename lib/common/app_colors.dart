import 'package:flutter/material.dart';

class AppColors {
  static final primary = Color(0xff539165);
  static final secondary = Color(0xff3F497F);
  static final accent = Color(0xffF7C04A);
  static final complement = Color(0xffF8F5E4);
  
  static final primary2 = Color(0xff00B8A9);
  static final secondary2 = Color(0xffF8F3D4);
  static final accent2 = Color(0xffF6416C);
  static final complement2 = Color(0xffFFDE7D);


}