import 'package:ads/common/navigator_service.dart';
import 'package:ads/common/utils.dart';
import 'package:ads/presentation/pages/splash_screen.dart';
import 'package:ads/presentation/provider/splash_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ads/injection.dart' as di;

import 'presentation/provider/ads_provider.dart';
import 'presentation/provider/login_provider.dart';
import 'presentation/provider/user_provider.dart';

class App extends StatelessWidget {
  const App({Key key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => di.locator<SplashNotifier>(),
        ),
        ChangeNotifierProvider(
          create: (_) => di.locator<LoginNotifier>(),
        ),
        ChangeNotifierProvider(
          create: (_) => di.locator<UserNotifier>(),
        ),
        ChangeNotifierProvider(
          create: (_) => di.locator<AdsNotifier>(),
        ),
      ],
      child: MaterialApp(
        navigatorKey: di.locator<NavigationService>().navigatorKey,
        title: 'Flutter Demo',
        navigatorObservers: [routeObserver],
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const SplashScreen(),
      ),
    );
  }
}
