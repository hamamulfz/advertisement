import 'package:ads/common/failure.dart';
import 'package:ads/domain/entities/advertisement.dart';
import 'package:dartz/dartz.dart';

abstract class AdsRepository {
  Future<Either<Failure, List<Advertisement>>> getAds();
}
