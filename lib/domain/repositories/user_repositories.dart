
import 'package:ads/common/failure.dart';
import 'package:ads/domain/entities/user_data.dart';
import 'package:dartz/dartz.dart';

abstract class UserRepository {
  Future<Either<Failure, UserData>> getUser();
}
