
import 'package:ads/common/failure.dart';
import 'package:ads/domain/entities/login_data.dart';
import 'package:dartz/dartz.dart';

abstract class AuthRepository {
  Future<Either<Failure, LoginData>> login(body);
}
