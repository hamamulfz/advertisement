import 'package:ads/common/failure.dart';
import 'package:ads/domain/entities/user_data.dart';
import 'package:ads/domain/repositories/user_repositories.dart';
import 'package:dartz/dartz.dart';


class GetUser {
  final UserRepository repository;

  GetUser(this.repository);


  Future<Either<Failure, UserData>> execute() {
    return repository.getUser();
  }
}
