import 'package:ads/common/failure.dart';

import 'package:ads/domain/entities/login_data.dart';

import 'package:ads/domain/repositories/auth_repositories.dart';

import 'package:dartz/dartz.dart';

class LoginAuth {
  final AuthRepository repository;

  LoginAuth(this.repository);

  Future<Either<Failure, LoginData>> execute(body) {
    return repository.login(body);
  }
}
