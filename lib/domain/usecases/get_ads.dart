import 'package:ads/common/failure.dart';
import 'package:ads/domain/entities/advertisement.dart';
import 'package:ads/domain/repositories/ads_repositories.dart';
import 'package:dartz/dartz.dart';


class GetAds {
  final AdsRepository repository;

  GetAds(this.repository);


  Future<Either<Failure, List<Advertisement>>> execute() {
    return repository.getAds();
  }
}
