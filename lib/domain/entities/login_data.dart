class LoginData {
  String timestamp;
  String statusCode;
  String token;
  String message;
  String error;

  LoginData(
      {this.timestamp, this.statusCode, this.token, this.message, this.error});

  LoginData.fromJson(Map<String, dynamic> json) {
    timestamp = json['timestamp'];
    statusCode = json['statusCode'];
    token = json['token'];
    message = json['message'];
    error = json['error'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['timestamp'] = this.timestamp;
    data['statusCode'] = this.statusCode;
    data['token'] = this.token;
    data['message'] = this.message;
    data['error'] = this.error;
    return data;
  }
}