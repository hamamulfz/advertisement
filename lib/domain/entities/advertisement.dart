import 'package:ads/common/date_helper.dart';

class Advertisement {
  int id;
  int version;
  String createDate;
  String lastUpdate;
  String name;
  String title;
  String imagePath;
  String body;
  String termsAndConditions;
  String recordStatus;
  String expiryDate;
  bool limitedToUserGroups;
  bool limitedToUsers;
  List<dynamic> allowedUserGroups;
  List<dynamic> allowedUsers;
  List<dynamic> allowedNationalities;
  bool limitedToNationality;
  int appearanceOrder;

  get formattedExpiryDate => DateHelper.convertDateFormat(expiryDate);

  Advertisement(
      {this.id,
      this.version,
      this.createDate,
      this.lastUpdate,
      this.name,
      this.title,
      this.imagePath,
      this.body,
      this.termsAndConditions,
      this.recordStatus,
      this.expiryDate,
      this.limitedToUserGroups,
      this.limitedToUsers,
      this.allowedUserGroups,
      this.allowedUsers,
      this.allowedNationalities,
      this.limitedToNationality,
      this.appearanceOrder});

  Advertisement.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    version = json['version'];
    createDate = json['createDate'];
    lastUpdate = json['lastUpdate'];
    name = json['name'];
    title = json['title'];
    imagePath = json['imagePath'];
    body = json['body'];
    termsAndConditions = json['termsAndConditions'];
    recordStatus = json['recordStatus'];
    expiryDate = json['expiryDate'];
    limitedToUserGroups = json['limitedToUserGroups'];
    limitedToUsers = json['limitedToUsers'];
    // if (json['allowedUserGroups'] != null) {
    //   allowedUserGroups = <Null>[];
    //   json['allowedUserGroups'].forEach((v) {
    //     allowedUserGroups!.add(new Null.fromJson(v));
    //   });
    // }
    // if (json['allowedUsers'] != null) {
    //   allowedUsers = <Null>[];
    //   json['allowedUsers'].forEach((v) {
    //     allowedUsers!.add(new Null.fromJson(v));
    //   });
    // }
    // if (json['allowedNationalities'] != null) {
    //   allowedNationalities = <Null>[];
    //   json['allowedNationalities'].forEach((v) {
    //     allowedNationalities!.add(new Null.fromJson(v));
    //   });
    // }
    limitedToNationality = json['limitedToNationality'];
    appearanceOrder = json['appearanceOrder'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['version'] = this.version;
    data['createDate'] = this.createDate;
    data['lastUpdate'] = this.lastUpdate;
    data['name'] = this.name;
    data['title'] = this.title;
    data['imagePath'] = this.imagePath;
    data['body'] = this.body;
    data['termsAndConditions'] = this.termsAndConditions;
    data['recordStatus'] = this.recordStatus;
    data['expiryDate'] = this.expiryDate;
    data['limitedToUserGroups'] = this.limitedToUserGroups;
    data['limitedToUsers'] = this.limitedToUsers;
    // if (this.allowedUserGroups != null) {
    //   data['allowedUserGroups'] =
    //       this.allowedUserGroups!.map((v) => v.toJson()).toList();
    // }
    // if (this.allowedUsers != null) {
    //   data['allowedUsers'] = this.allowedUsers!.map((v) => v.toJson()).toList();
    // }
    // if (this.allowedNationalities != null) {
    //   data['allowedNationalities'] =
    //       this.allowedNationalities!.map((v) => v.toJson()).toList();
    // }
    data['limitedToNationality'] = this.limitedToNationality;
    data['appearanceOrder'] = this.appearanceOrder;
    return data;
  }
}
