
class UserData {
  String userName;
  String photo;
  String gender;
  String nationality;

  UserData({this.userName, this.photo, this.gender, this.nationality});

  UserData.fromJson(Map<String, dynamic> json) {
    userName = json['userName'];
    photo = json['photo'];
    gender = json['gender'];
    nationality = json['nationality'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userName'] = this.userName;
    data['photo'] = this.photo;
    data['gender'] = this.gender;
    data['nationality'] = this.nationality;
    return data;
  }
}