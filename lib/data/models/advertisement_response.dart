import 'package:ads/domain/entities/advertisement.dart';

class AdvertisementResponse {
  String statusCode;
  String timestamp;
  String message;
  List<Advertisement> payload;

  AdvertisementResponse(
      {this.statusCode, this.timestamp, this.message, this.payload});

  AdvertisementResponse.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    timestamp = json['timestamp'];
    message = json['message'];
    if (json['payload'] != null) {
      payload = <Advertisement>[];
      json['payload'].forEach((v) {
        payload.add(new Advertisement.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['timestamp'] = this.timestamp;
    data['message'] = this.message;
    if (this.payload != null) {
      data['payload'] = this.payload.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
