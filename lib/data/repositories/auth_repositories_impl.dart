import 'dart:io';

import 'package:ads/common/exception.dart';
import 'package:ads/common/failure.dart';
import 'package:ads/data/datasources/auth_datasource.dart';
import 'package:ads/domain/entities/login_data.dart';
import 'package:ads/domain/repositories/auth_repositories.dart';

import 'package:dartz/dartz.dart';

class AuthRepositoryImpl implements AuthRepository {
  final AuthDataSource dataSource;

  AuthRepositoryImpl({this.dataSource});

  @override
  Future<Either<Failure, LoginData>>  login(body) async {
    try {
      final result = await dataSource.login(body);

      return Right(result);
    } on ServerException {
      return Left(ServerFailure(''));
    } on SocketException {
      return Left(ConnectionFailure('Failed to connect to the network'));
    }
  }
}
