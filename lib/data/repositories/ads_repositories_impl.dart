import 'dart:io';

import 'package:ads/common/exception.dart';
import 'package:ads/common/failure.dart';
import 'package:ads/data/datasources/ads_datasource.dart';
import 'package:ads/domain/entities/advertisement.dart';
import 'package:ads/domain/repositories/ads_repositories.dart';
import 'package:dartz/dartz.dart';

class AdsRepositoryImpl implements AdsRepository {
  final AdsDataSource dataSource;

  AdsRepositoryImpl({this.dataSource});

  @override
  Future<Either<Failure, List<Advertisement>>> getAds() async {
    try {
      
      final result = await dataSource.getAds();

      return Right(result);
    } on ServerException {
      return Left(ServerFailure(''));
    } on SocketException {
      return Left(ConnectionFailure('Failed to connect to the network'));
    }
  }
}
