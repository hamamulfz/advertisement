import 'dart:io';

import 'package:ads/common/exception.dart';
import 'package:ads/common/failure.dart';
import 'package:ads/data/datasources/user_datasource.dart';
import 'package:ads/domain/entities/user_data.dart';

import 'package:ads/domain/repositories/user_repositories.dart';
import 'package:dartz/dartz.dart';

class UserRepositoryImpl implements UserRepository {
  final UserDataSource dataSource;

  UserRepositoryImpl({this.dataSource});

  @override
  Future<Either<Failure, UserData>> getUser() async {
    try {

      
      final result = await dataSource.getUser();

      return Right(result);
    } on ServerException {
      return Left(ServerFailure(''));
    } on SocketException {
      return Left(ConnectionFailure('Failed to connect to the network'));
    }
  }
}
