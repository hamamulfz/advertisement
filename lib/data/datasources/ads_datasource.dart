import 'dart:convert';

import 'package:ads/common/api_urls.dart';
import 'package:ads/common/exception.dart';
import 'package:ads/common/navigator_service.dart';
import 'package:ads/common/pref_manager.dart';
import 'package:ads/data/models/advertisement_response.dart';
import 'package:ads/domain/entities/advertisement.dart';
import 'package:ads/injection.dart';
import 'package:ads/presentation/pages/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

abstract class AdsDataSource {
  Future<List<Advertisement>> getAds();
}

class AdsDataSourceDataSourceImpl implements AdsDataSource {
  final http.Client client;

  AdsDataSourceDataSourceImpl({@required this.client});

  @override
  Future<List<Advertisement>> getAds() async {
    var _initPrefManager = await SharedPreferences.getInstance();
    final pref = PrefManager(_initPrefManager);
    final body = pref.getTokenBody();

    final response = await client.post(
      Uri.parse(ApiUrl.getAdvertisment),
      body: body,
    );

    if (response.statusCode == 200) {
      return AdvertisementResponse.fromJson(jsonDecode(response.body)).payload;
    } else if (response.statusCode == 401) {
      locator<NavigationService>().navigateAndReplaceAllTo(LoginScreen());
      throw Unauthenticated();
    } else {
      throw ServerException();
    }
  }
}
