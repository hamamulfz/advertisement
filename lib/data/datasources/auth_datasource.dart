import 'dart:convert';

import 'package:ads/common/api_urls.dart';
import 'package:ads/common/exception.dart';
import 'package:ads/domain/entities/login_data.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

abstract class AuthDataSource {
  Future<LoginData> login(body);
}

class AuthDataSourceDataSourceImpl implements AuthDataSource {
  final http.Client client;

  AuthDataSourceDataSourceImpl({@required this.client});

  @override
  Future<LoginData> login(body) async {
    final response = await client.post(
      Uri.parse(ApiUrl.login),
      body: body
    );

    if (response.statusCode == 200) {
      return LoginData.fromJson(jsonDecode(response.body));
    } else if (response.statusCode == 401) {
      throw Unauthenticated();
    } else {
      throw ServerException();
    }
  }
}
