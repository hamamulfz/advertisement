import 'dart:convert';

import 'package:ads/common/api_urls.dart';
import 'package:ads/common/exception.dart';
import 'package:ads/common/navigator_service.dart';
import 'package:ads/common/pref_manager.dart';
import 'package:ads/data/models/user_response.dart';
import 'package:ads/domain/entities/user_data.dart';
import 'package:ads/presentation/pages/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../injection.dart';

abstract class UserDataSource {
  Future<UserData> getUser();
}

class UserDataSourceDataSourceImpl implements UserDataSource {
  final http.Client client;

  UserDataSourceDataSourceImpl({@required this.client});

  @override
  Future<UserData> getUser() async {
    var _initPrefManager = await SharedPreferences.getInstance();
    final pref = PrefManager(_initPrefManager);
    final body = pref.getTokenBody();
    final response = await client.post(
      Uri.parse(ApiUrl.getUser),
      body: body,
    );

    if (response.statusCode == 200) {
      return UserResponse.fromJson(jsonDecode(response.body)).data;
    } else if (response.statusCode == 401) {
      locator<NavigationService>().navigateAndReplaceAllTo(LoginScreen());
      throw Unauthenticated();
    } else {
      throw ServerException();
    }
  }
}
