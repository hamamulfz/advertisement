# ads

A MoolahGo CodingTest in recruitment process

## Getting Started

This project is flutter application that runs on following environment:

```bash
Flutter 1.17.5
```

You can use FVM or rollback to this version in order to run this code.

```bash
fvm use 1.17.5
fvm list
```

## How to run

clone this repo using following command:
```bash
git clone https://gitlab.com/hamamulfz/advertisement.git
```


to run debug, use following command

```bash
fvm flutter run
```

to build apk, use following command

```bash
fvm flutter build apk
```

or

```bash
fvm flutter build appbundle
```

## Feature

the feature included in this application is the following:

- [x] Splash Screen
- [x] Login Screen
- [x] Home Screen
- [x] Ads Detail Screen
- [x] Profile Screen
- [x] Profile Detail Screen
- [x] Ads Detail Screen

<table>
    <tr>
        <td> 
            <img src="ss/splash.png"  alt="1" width = 360px height = 640px>
        </td>
        <td>
            <img src="ss/login.png" alt="2" width = 360px height = 640px>
        </td>
    </tr> 
   <tr>
        <td>
            <img src="ss/home.png" alt="3" width = 360px height = 640px>
        </td>
        <td>
            <img src="ss/detail.png" align="right" alt="4" width = 360px height = 640px>
        </td>
    </tr>
   <tr>
        <td>
            <img src="ss/profile.png" alt="3" width = 360px height = 640px>
        </td>
        <td>
            <img src="ss/profile_detail.png" align="right" alt="4" width = 360px height = 640px>
        </td>
    </tr>
</table>


## Disclaimer:

All icon are taken from flaticon.com